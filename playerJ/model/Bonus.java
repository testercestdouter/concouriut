package model;

/**
 * Created by Valentin on 11/03/2017.
 */
public class Bonus extends AbstractCase {
    protected BonusType type;

    public Bonus(int ligne, int colonne, BonusType type) {
        super(new Position(ligne, colonne));
        this.type = type;
    }

    public Bonus(Position position, BonusType type) {
        super(position);
        this.type = type;
    }

    public BonusType getType() {
        return type;
    }

    public void setType(BonusType type) {
        this.type = type;
    }


    @Override
    public String toString() {
        switch (type) {
            case Bombes:
                return "%";
            case Points:
                return "$";
            case Explosion:
                return "#";
            case Téléportation:
                return "@";
            default:
                return "I";
        }
    }
}
