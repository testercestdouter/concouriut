package model;

/**
 * Created by Valentin on 11/03/2017.
 */
public enum Ordre {
    Nord ("N"),
    Sud ("S"),
    Ouest ("O"),
    Est ("E"),
    Immobile(" "),
    Bombe ("B");

    private final String name;

    private Ordre(String s) {
        name = s;
    }

    public String toString() {
        return this.name;
    }
}
