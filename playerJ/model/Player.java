package model;

/**
 * Created by Valentin on 11/03/2017.
 */
public class Player extends AbstractCase {
    protected char id;
    private int nbBombe;


    public Player(int ligne, int colonne, char id, int nbBombe) {
        super(new Position(ligne, colonne));
        this.id = id;
        this.nbBombe = nbBombe;
    }

    public Player(Position position, char id, int nbBombe) {
        super(position);
        this.id = id;
        this.nbBombe = nbBombe;
    }

    public char getId() {
        return id;
    }

    public void setId(char id) {
        this.id = id;
    }

    public int getNbBombe() {
        return nbBombe;
    }

    public void decrBombe() {
        nbBombe--;
    }

    @Override
    public String toString() {
        return id+"";
    }

    public void setPosition(int ligne, int colonne) {
        position.setLigne(ligne);
        position.setColonne(colonne);
    }
}
