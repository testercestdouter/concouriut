package model;

/**
 * Created by Valentin on 11/03/2017.
 */
public abstract class AbstractCase
{
    protected Position position;

    public AbstractCase(Position position) {

        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AbstractCase abstractCase = (AbstractCase) o;

        return position != null ? position.equals(abstractCase.position) : abstractCase.position == null;

    }

    @Override
    public int hashCode() {
        return position != null ? position.hashCode() : 0;
    }

    public Position getPosition() {

        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
