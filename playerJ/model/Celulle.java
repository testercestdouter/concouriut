package model;

/**
 * Created by Valentin on 11/03/2017.
 */
public class Celulle {
    private AbstractCase entity;

    public Celulle() {
    }

    public Celulle(AbstractCase entity) {
        this.entity = entity;
    }

    public AbstractCase getEntity() {
        return entity;
    }

    public void setEntity(AbstractCase entity) {
        this.entity = entity;
    }

    public boolean isWalkable() {
        return entity == null || entity instanceof Bonus;
    }

    @Override
    public String toString() {
        return entity == null ? " " : entity.toString();
    }
}
