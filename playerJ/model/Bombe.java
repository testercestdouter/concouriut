package model;

/**
 * Created by Valentin on 11/03/2017.
 */
public class Bombe extends AbstractCase {
    public final static int DEFAULT_TOURSRESTANT = 4;

    protected int toursRestant;

    public Bombe(int ligne, int colonne) {
        super(new Position(ligne, colonne));
        toursRestant = DEFAULT_TOURSRESTANT;
    }

    public Bombe(Position position) {
        super(position);
        toursRestant = DEFAULT_TOURSRESTANT;
    }

    public void decrementerTourRestant() {
        toursRestant--;
    }

    public int getToursRestant() {
        return toursRestant;
    }

    @Override
    public String toString() {
        return "B";
    }
}
