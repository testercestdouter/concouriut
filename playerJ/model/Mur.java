package model;

/**
 * Created by Valentin on 11/03/2017.
 */
public class Mur extends AbstractCase {


    public Mur(int ligne, int col) {
        super(new Position(ligne, col));
    }

    public Mur(Position position) {
        super(position);
    }

    @Override
    public String toString() {
        return "X";
    }
}
