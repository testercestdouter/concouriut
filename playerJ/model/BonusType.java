package model;

/**
 * Created by whiteshad on 11/03/17.
 */
public enum BonusType {
    Bombes,
    Points,
    Explosion,
    Téléportation
}