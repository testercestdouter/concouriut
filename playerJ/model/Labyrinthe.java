package model;

import java.util.*;

/**
 * Created by gaeta on 11/03/2017.
 */
public class Labyrinthe {
    private static final String lineSeparator = System.getProperty("line.separator");
    private List<Player> players;
    private List<Bombe> bombes;
    private List<Bonus> bonusList;
    private Celulle[][] laGrilleDeFolie;
    private int nbligne;
    private int nbColonne;
    private List<Position> voisinsLibres;
    private short[][] safeTab;

    public Labyrinthe(int nbligne, int nbColonne) {
        this.nbligne = nbligne;
        this.nbColonne = nbColonne;
        laGrilleDeFolie = new Celulle[nbligne][nbColonne];
        safeTab = new short[nbligne][nbColonne];
        for (int i = 0; i < laGrilleDeFolie.length; i++) {
            for (int j = 0; j < laGrilleDeFolie[i].length; j++) {
                laGrilleDeFolie[i][j] = new Celulle();
            }
        }
        bombes = new ArrayList<>();
        players = new ArrayList<>();
        bonusList = new ArrayList<>();
    }

    public String toString() {
        String s = "";
        for (Celulle[] ligne : laGrilleDeFolie) {
            s += "|";
            for (Celulle celulle : ligne) {
                s += celulle + "|";
            }
            s += lineSeparator;
        }
        return s;
    }

    public void calculerSafeTab() {
        for (int i = 0; i < safeTab.length; i++) {
            for (short j : safeTab[i]) {
                safeTab[i][j] = 0;
            }
        }
        int max = proximiteBonusExplosion();
        int lig;
        int col;
        for (Bombe bombe : bombes) {
            lig = bombe.position.getLigne();
            col = bombe.position.getColonne();
            for (int i = 0; i < 4; ++i) {
                int newCol = (col + i) % nbColonne;
                if (laGrilleDeFolie[lig][newCol].getEntity() instanceof Mur)
                    break;
                else
                    safeTab[lig][newCol] = (short) Math.min(bombe.getToursRestant(), max);
            }
            for (int i = 0; i < 4; ++i) {
                int newLig = (lig + i) % nbligne;
                if (laGrilleDeFolie[newLig][col].getEntity() instanceof Mur)
                    break;
                else
                    safeTab[newLig][col] = (short) Math.min(bombe.getToursRestant(), max);
            }
            for (int i = 0; i < 4; ++i) {
                int newCol = Math.floorMod(col - i, nbColonne);
                if (laGrilleDeFolie[lig][newCol].getEntity() instanceof Mur)
                    break;
                else
                    safeTab[lig][newCol] = (short) Math.min(bombe.getToursRestant(), max);
            }
            for (int i = 0; i < 4; ++i) {
                int newLig = Math.floorMod(lig - i, nbligne);
                if (laGrilleDeFolie[newLig][col].getEntity() instanceof Mur)
                    break;
                else
                    safeTab[newLig][col] = (short) Math.min(bombe.getToursRestant(), max);
            }
        }
    }

    public void printSafeTab() {
        //todo : debug print
        for (short[] shorts : safeTab) {
            for (short cel : shorts) {
                System.out.print(cel);
            }
            System.out.println();
        }
    }

    public List<Position> regardeTuPeuxAllerLa(int x, int y) {
        List<Position> retour = new ArrayList<>();
        if (this.laGrilleDeFolie[y][Math.floorMod(x + 1, nbColonne)].isWalkable())
            retour.add(new Position(y, Math.floorMod(x + 1, nbColonne)));

        if (this.laGrilleDeFolie[y][Math.floorMod(x - 1, nbColonne)].isWalkable())
            retour.add(new Position(y, Math.floorMod(x - 1, nbColonne)));

        if (this.laGrilleDeFolie[Math.floorMod(y + 1, nbligne)][x].isWalkable())
            retour.add(new Position(Math.floorMod(y + 1, nbligne), x));

        if (this.laGrilleDeFolie[Math.floorMod(y - 1, nbligne)][x].isWalkable())
            retour.add(new Position(Math.floorMod(y - 1, nbligne), x));

        return retour;
    }

    public int proximiteBonusExplosion() {
        return players.parallelStream()
                .mapToInt(player ->
                        bonusList.parallelStream()
                                .filter(x -> x.getType() == BonusType.Explosion)
                                .mapToInt(x -> aStar(player.position, x.position).size() - 1)
                                .min().orElse(5))
                .min().orElse(5);
    }

    public void printBonusList() {
        if (bonusList.isEmpty())
            System.out.println("Empty");
        else
            bonusList.forEach(x -> System.out.println(x + " " + x.position));
    }

    public Bonus getNearestBonus(Player player) {
        Comparator<Bonus> comp = (x, y) -> {
            int distX = aStar(player.position, x.position).size() - 1;
            int distY = aStar(player.position, y.position).size() - 1;
            return distX > distY ? 1 : distX == distY ? 0 : -1;
        };
        return bonusList.parallelStream().sorted(comp).findFirst().orElseGet(() -> null);
    }

    public Player getPlayer(char id) {
        return players.stream().filter(player -> player.getId() == id).findFirst().get();
    }

    public void addMur(int i, int j) {
        if(laGrilleDeFolie[i][j].getEntity()!=null)
            remove(i,j);
        if (!(laGrilleDeFolie[i][j].getEntity() instanceof Mur)) {
            laGrilleDeFolie[i][j].setEntity(new Mur(i, j));
        }
    }

    public void addBonus(int i, int j, BonusType bonusType) {
        if(laGrilleDeFolie[i][j].getEntity()!=null)
            remove(i,j);
        if (!(laGrilleDeFolie[i][j].getEntity() instanceof Bonus)) {
            Bonus b = new Bonus(i, j, bonusType);
            bonusList.add(b);
            laGrilleDeFolie[i][j].setEntity(b);
        }
    }

    public void remove(int i, int j) {
        AbstractCase pos = laGrilleDeFolie[i][j].getEntity();
        if (pos instanceof Bonus) {
            bonusList.remove(pos);
        } else if (pos instanceof Bombe) {
            bombes.remove(pos);
        }
        laGrilleDeFolie[i][j].setEntity(null);
    }

    public void movePlayer(int i, int j, char c) {
        if(laGrilleDeFolie[i][j].getEntity()!=null)
            remove(i,j);
        Player player = players.parallelStream().filter(x -> x.getId() == ((int) c)).findFirst().orElseGet(() -> {
            Player p = new Player(0, 0, c, 10);
            players.add(p);
            return p;
        });
        laGrilleDeFolie[i][j].setEntity(player);
        player.setPosition(i, j);
    }

    private int manhattan(Position from, Position to) {
        int fromX = from.colonne;
        int fromY = from.ligne;
        int toX = to.colonne;
        int toY = to.ligne;

        return Math.abs(toX - fromX) + Math.abs(toY - fromY);
    }

    private int realManhattan(Position from, Position to) {
        int fromX = from.colonne;
        int fromY = from.ligne;
        int toX = to.colonne;
        int toY = to.ligne;

        int normal = manhattan(from, to);

        Position fromWidth = new Position(fromY + nbColonne, fromX);
        int width = manhattan(fromWidth, to);

        Position fromHeight = new Position(fromY, fromX + nbligne);
        int height = manhattan(fromHeight, to);

        Position fromWidthHeight = new Position(fromY + nbligne, fromX + nbColonne);
        int widthHeight = manhattan(fromWidthHeight, to);

        int[] allDistances = new int[]{normal, width, height, widthHeight};
        return Arrays.stream(allDistances).min().getAsInt();
    }

    public List<Position> aStar(Position from, Position to) {
        List<Position> frontier = new ArrayList<>();
        Map<Position, Position> cameFrom = new HashMap<>();
        Map<Position, Integer> costSoFar = new HashMap<>();
        Map<Position, Integer> priority = new HashMap<>();

        cameFrom.put(from, null);
        costSoFar.put(from, 0);
        frontier.add(from);
        priority.put(from, 0);
        int maxiter = 500;

        while (!frontier.isEmpty()) {
            --maxiter;
            Position current = frontier.get(0);
            frontier.remove(0);


            if (current.equals(to)) {
                break;
            }

            List<Position> neighbors = regardeTuPeuxAllerLa(current.colonne, current.ligne);
            for (Position next : neighbors) {
                int newCost = costSoFar.get(current) + 1;
                if (!costSoFar.containsKey(next) || newCost < costSoFar.get(next)) {

                    costSoFar.put(next, newCost);
                    int costHeuristic = newCost + realManhattan(next, to);
                    frontier.add(next);
                    cameFrom.put(next, current);
                    priority.put(next, costHeuristic);

                }
            }
            frontier.sort(Comparator.comparing(priority::get));
        }

        List<Position> path = new ArrayList<>();
        Position current = to;
        while (current != null) {
            path.add(0, current);
            current = cameFrom.get(current);
        }
        return path;
    }

    public boolean isSafe(Position pos) {
        return isSafe(pos, 0);
    }

    public boolean isSafe(Position pos, int lvl) {
        return safeTab[pos.ligne][pos.colonne] <= lvl;
    }

    public boolean isSafe(int ligne, int col, int lvl) {
        return safeTab[ligne][col] <= lvl;
    }

    public short getSafeLevel(int ligne, int colonne) {
        return safeTab[ligne][colonne];
    }

    public void addBombe(int ligne, int colonne) {
        if(laGrilleDeFolie[ligne][colonne].getEntity()!=null)
            remove(ligne,colonne);
        if (laGrilleDeFolie[ligne][colonne].getEntity() instanceof Bombe) {
            ((Bombe) laGrilleDeFolie[ligne][colonne].getEntity()).decrementerTourRestant();
        } else {
            Bombe b = new Bombe(ligne, colonne);
            bombes.add(b);
            laGrilleDeFolie[ligne][colonne].setEntity(b);
        }
    }

    public List<Bombe> getBombes() {
        return bombes;
    }

    public void updateVoisinsLibres(char id) {
        Player p = getPlayer(id);
        Position pos = p.position;
        voisinsLibres = regardeTuPeuxAllerLa(pos.colonne, pos.ligne);
    }

    public void addMyPlayer(Player player) {
        players.add(player);
    }

    public boolean isWalkable(int ligne, int colonne) {
        return laGrilleDeFolie[ligne][colonne].isWalkable();
    }
}
