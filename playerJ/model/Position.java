package model;

/**
 * Created by Valentin on 11/03/2017.
 */
public class Position {
    int ligne;
    int colonne;

    public Position(int ligne, int colonne) {
        this.ligne = ligne;
        this.colonne = colonne;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Position position = (Position) o;

        if (ligne != position.ligne) return false;
        return colonne == position.colonne;

    }

    @Override
    public int hashCode() {
        int result = ligne;
        result = 31 * result + colonne;
        return result;
    }

    public int getLigne() {
        return ligne;
    }

    public void setLigne(int ligne) {
        this.ligne = ligne;
    }

    public int getColonne() {
        return colonne;
    }

    public void setColonne(int colonne) {
        this.colonne = colonne;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Position{");
        sb.append("ligne=").append(ligne);
        sb.append(", colonne=").append(colonne);
        sb.append('}');
        return sb.toString();
    }
}
