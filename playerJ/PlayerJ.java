import model.*;

import java.util.Random;

public class PlayerJ {

    private static String ordres = "NSEOB";
    private int nbLig;
    private int nbCol;
    private Network net;
    private char ID;
    private String login;

    private Labyrinthe laby;
    private boolean mortSubite;
    private Random generateur;
    private Player player;

    public PlayerJ(String h, int p, String l) {
        net = new Network();
        net.connectTo(h, p);
        login = l;
        generateur = new Random();
    }

    public static void main(String[] args) {
//        Labyrinthe laby = new Labyrinthe(8, 6);
//        Player myPlayer=new Player(0,0,'1',10);
//        laby.addMyPlayer(myPlayer);
//        laby.movePlayer(0, 0, '1');
//        laby.movePlayer(2, 3, '2');
//        laby.addBombe(6, 0);
//        laby.addBonus(3, 2, BonusType.Explosion);
//        laby.addMur(1, 0);
//        laby.addMur(0, 1);
//        laby.addMur(0, 4);
//        laby.addMur(1, 4);
//        laby.addMur(1, 3);
//        laby.addMur(2, 2);
//        laby.addMur(3, 5);
//        laby.addMur(3, 4);
//        laby.addMur(4, 3);
//        System.out.println(laby);
//        laby.remove(3,2);
//        System.out.println(laby.getNearestBonus(myPlayer));
        PlayerJ p = new PlayerJ(args[0], Integer.parseInt(args[1]), args[2]);
        p.connexionServeur();
        p.demarrer(args[2]);
    }

    public Ordre farmBonus() throws Exception {
        System.out.println("Je suis à "+player.getPosition().toString());
        Bonus cible = laby.getNearestBonus(player);
        if(cible==null){
            return Ordre.Immobile;
        }
        System.out.println("Ma cible est "+cible.getPosition().toString());
        Position toGo = laby.aStar(player.getPosition(), cible.getPosition()).get(1);
        return orderFromPosition(player.getPosition(),toGo);
    }

    private Ordre orderFromPosition(Position from, Position to) throws Exception {
        int ligFrom = from.getLigne();
        int colFrom = from.getColonne();
        int colTo = to.getColonne();
        int ligTo = to.getLigne();

        int s = (ligFrom + 1) % nbLig;
        int e = (colFrom + 1) % nbCol;
        int n = Math.floorMod(ligFrom - 1, nbLig);
        int o = Math.floorMod(colFrom - 1, nbCol);
        s-=ligTo;
        n-=ligTo;
        e-=colTo;
        o-=colTo;
        if (s == 0) {
            return Ordre.Sud;
        } else if (n == 0) {
            return Ordre.Nord;
        } else if (e == 0) {
            return Ordre.Est;
        } else if (o == 0) {
            return Ordre.Ouest;
        } else
            return Ordre.Immobile;

    }

    public void lireSpecLaby() throws Exception {
        ID = net.getChar();
        nbLig = net.getInt();
        nbCol = net.getInt();
        String typePartie = net.getString();
        mortSubite = "ms".equals(typePartie);
        laby = new Labyrinthe(nbLig, nbCol);
        player = new Player(nbLig, nbCol, ID, 10);
        laby.addMyPlayer(player);
    }

    public void lireLaby() throws Exception {
        for (int i = 0; i < nbLig; i++)
            for (int j = 0; j < nbCol; j++) {
                char c = net.getChar();
                switch (c) {
                    case 'X':
                        laby.addMur(i, j);
                        break;
                    case '%':
                        laby.addBonus(i, j, BonusType.Bombes);
                        break;
                    case '$':
                        laby.addBonus(i, j, BonusType.Points);
                    case '#':
                        laby.addBonus(i, j, BonusType.Explosion);
                        break;
                    case '@':
                        laby.addBonus(i, j, BonusType.Téléportation);
                        break;
                    case ' ':
                        laby.remove(i, j);
                        break;
                    default:
                        laby.movePlayer(i, j, c);
                        break;
                }
            }
        // lecture bombes
        int nbBombes = net.getInt();
        for (int i = 0; i < nbBombes; i++) {
            laby.addBombe(net.getInt(), net.getInt());
        }
        laby.updateVoisinsLibres(this.ID);
        laby.calculerSafeTab();

    }


    public void connexionServeur() {
        try {
            String msg = "LOGIN " + login;
            net.sendString(msg);
            String s = net.getString();
            if (s.startsWith("SPEC")) {
                lireSpecLaby();
            } else {
                net.disconnect();
            }
        } catch (Exception e) {
            System.exit(0);
        }

    }

    public void demarrer(String login) {
        boolean encore = true;
        while (encore) {
        try {
            //init map
            String s = net.getString();
            System.out.println("Message: " + s);
            if (s.startsWith("MAP")) {
                lireLaby();
                net.sendChar(farmBonus().toString().charAt(0));

            } else if (s.startsWith("DEAD") || s.startsWith("QUIT")) {
                encore = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
         }
        net.disconnect();
    }

    private char randomOrdre(int max) {
        int c;
        boolean ok = false;
        int ligne = player.getPosition().getLigne();
        int col = player.getPosition().getColonne();
        do {
            c = generateur.nextInt(max);
            //NSEO
            switch (c) {
                case 0:
                    ok = laby.isWalkable(Math.floorMod(ligne - 1, nbLig), col);
                    break;
                case 1:
                    ok = laby.isWalkable((ligne + 1) % nbLig, col);
                    break;
                case 2:
                    ok = laby.isWalkable(ligne, (col + 1) % nbCol);
                    break;
                case 3:
                    ok = laby.isWalkable(ligne, Math.floorMod(col - 1, nbCol));
                    break;
                case 4:
                    ok = true;
                    break;
            }
        } while (!ok);
        return ordres.charAt(c);
    }


}
